    %EXPS: Compute the exponential function using its Taylor series.
%
%Usage:
%
%  >> y=exps(x);
%
function [y]=exps(x)
  

x2=x;       % Doesn't yet serve a purpose. This is a preparation for 
            % a change in the program.
            
k=0;term=1; 
S1=1;S0=0;  % Two consecutive partial sums.

while (S1~=S0),
  k=k+1;              % Compute the current term using the previous one.
  term=term*x2/k;     % This saves computational work as well as avoids
                      % overflow as both x^k and k! may be too large but
                      % the quotient is resonably sized.
  S0=S1;S1=S1+term;  
end;
	     
y=S1;                % Save the result in the y-variable.


if (1 == 0),         % Also preparation for a possible change.
  y=y;              
end;               
                   
