    % fix point iteration 4.1
% f(x) = cos(x)^2 - x;
% >> y=fp_cos2(x);
% where x is the starting position

function [y]=fp_cos2(x)

x1 = x;         % initial fixed point
old_x = x1;
k = 0;          % iteration counter
er = 10;        % error
while (er > 0.000001)
    x1 = (cos(old_x))^2;
    k = k + 1;
    if k > 999
        break;
    end
    er = abs(x1-old_x);
    old_x = x1;
end

y = x1;
display(k);
ezplot(@(x1) x1, [0 1.5])
hold on
ezplot(@(x1) cos(x1)^2, [0 1.5])
ylim([0 1.2])
end