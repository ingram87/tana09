%EXPS: Compute the exponential function using its Taylor series.
%
%Usage:
%
%  >> y=expsn(x);
%
function [y]=expsn(x)
  

x2=x;       % Doesn't yet serve a purpose. This is a preparation for 
            % a change in the program.
            
y = 1/exps(x);
            
end               
                   
