% fix point iteration 4.1
% f(x) = cos(x)^2 - x;
% >> y=fp_arccos(x);
% where x is the starting position

function [y]=fp_arccos(x)

x1 = x;         % initial fixed point
old_x = x1;
k = 0;          % iteration counter
er = 10;        % error
while (er > 0.000001)
    x1 = acos(sqrt(old_x));
    k = k + 1;
    if k > 999
        break;
    end
    er = abs(x1-old_x);
    old_x = x1;
end

y = x1;
display(k);
ezplot(@(x1) x1, [0 1.1])
hold on
ezplot(@(x1) acos(sqrt(x1)), [0 1.1])
ylim([0 1.2])
end
