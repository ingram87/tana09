function [ z ] = Division( x, y )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%a=y;
%f=inline('a-1/z','z');
%df=inline('1/(z^2)','z');
%[c,e] = NewtonSolv(1, f, df, 0.001);
%z = x*c;

x0 = 3/4;
for n=1:6,
    x1 = x0 .* (2 - y.* x0);
    x0 = x1;
end
z = x * x0;
