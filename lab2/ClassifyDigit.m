
function [Type] = ClassifyDigit(S, TheSubspaces)

min_d = 1000000000;
for j = 0:9
    d = DistanceFromSubspace(TheSubspaces{j + 1}, S);
    if d < min_d;
        min_d = d;
        Type = j;
    end
end

