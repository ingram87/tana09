
function [d] = DistanceFromSubspace(U, TestDigit)

[Q,R]=qr(U);
C = R \ (Q' * TestDigit);
r = TestDigit - U * C;
d = norm(r);

