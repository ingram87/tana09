format long;

A = [1 2 3; 2 3 4; 4 5 7];
[L, R, P] = lu(A)

x = ones(3,1);

b = A * x;
y = L \ P * b;
x_approx = R \ y;
x_diff = x - x_approx

t = (1:1455)';
m = 1455/2; % Middle of the range is the best
A = [(t-m).^0 (t-m) (t-m).^2];
cond(A);


C = A' * A;
Ab = A' *  EluxB;

c = C \ Ab;


figure
plot(t, A*c)
hold on
plot(t, EluxB)

tt = (1:1730)';
tm = 1730 / 2;
A2 = [(tt-tm).^0 (tt-tm) (tt-tm).^2];

figure
plot(tt, A2*c)
hold on
plot(tt, EluxB_new)

t3 = (1:1730)';
m3 = 1730/2;
vt=(2*pi*(t3-m3))./365;
A3 = [ones(1730,1), t3 - t3.*m3, sin(vt), cos(vt), sin(2*vt), cos(2*vt), sin(3*vt), cos(3*vt)];
C3=A3' * A3;
Ab3 = A3' * EluxB_new;

c3=C3 \ Ab3;


figure
plot(t3,A3*c3);
hold on
plot(t3, EluxB_new)



%A = [(tt-tm).^0 (tt-tm) (tt-tm).^2];
%pp = A*c;
%C = A' * A;
%Ab = A' *  EluxB;

%c = C \ Ab;
%pp = [(tt-tm).^0 (tt-tm) (t-tm).^2]*c;


%res = 1000000000;
%min_m = -1;
%for m=1:10000,
    %A = [(t-m).^0 (t-m) (t-m).^2];
    %ans = cond(A);
    %if ans < res
        %res = ans;
        %min_m = m;
    %end
    %res = min(res, cond(A));
%end
%res
%min_m

%e0 = 1/4;
%for n=1:7,
   %e1 = 2 * e0 * e0;
   %e0 = e1;
   %n
   %e0
%end
