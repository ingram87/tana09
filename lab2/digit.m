
k = 25;
TheSubspaces = CreateSubspace(RefSet, RefAns, k);

% Calculate correctness
num_tot = 0;
num_correct = 0;
for i = 1:columns(TestSet)
    num = TestSet(:,i);
    t = ClassifyDigit(num, TheSubspaces);
    if t == TestAns(i)
        ++num_correct;
    end
    ++num_tot;
end

printf("%d of %d correctly identified. %.2f%% accurracy.\n",
    num_correct, num_tot, num_correct / num_tot * 100);

