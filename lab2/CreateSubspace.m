
function [TheSubspaces] = CreateSubspace(RefSet, RefAns, k)

TheSubspaces = cell(10,1);

for j = 0:9
    % Rj contains only numbers where
    % RefAns(x) == j
    id = (RefAns(:) == j);
    Rj = RefSet(:, id);
    [Uj, Sj, Vj] = svd(Rj);
    TheSubspaces{j + 1} = Uj(:, 1 : k);
end

% Can inspect by
% DisplayDigit(TheSubspaces{1}(:, 1))
% DisplayDigit(TheSubspaces{1}(:, 2))
% etc...

