function [ sx, sy ] = ParametricCurve( x, y )
% some vector for evaluate the interpolation
n=10*length(x);
tt=(0:n)/n;
% interpolation on both x and y with parameter interval 0<=t<=1
t=0:1/(length(x)-1):1;
t=t';
sx=csape(t,x);
sy=csape(t,y);
% graph
plot(ppval(sx,tt), ppval(sy,tt));
end

