%% Exercise 2.1
t=[1,1.5];
y=[1.6602,1.7383];
p=polyfit(t,y,1);
tt=1.3;
yy=polyval(p,tt);
fprintf ('P_1(1.3) = %d \n', yy);

%% Exercise 2.3
figure
hold on;
RungesPhenomena(4);
RungesPhenomena(6);
RungesPhenomena(8);
hold off;

%% Exercise 3.1
t31=[0,2,4,5,6];
y31=[0,0,1,0,0];
pn31=csape(t31,y31)
tt31=0:0.01:6;
yy31=fnval(pn31,tt31);
plot(tt31,yy31,'red');

%% Exercise 3.2
pr32=csape(t31,y31,[0,0]);
yy32=fnval(pr32,tt31);
hold on;
plot(tt31,yy32,'blue');
hold off;

%% Exercise 4.2
% the best i can do is with 10 points... 
DisplayMap();
Points=AddPointsToMap(10);
x41=Points(:,1);
y41=Points(:,2);
[sx,sy]=ParametricCurve(x41,y41);


%% Exercise 4.3
L = CalculateLength(sx, sy, 100);
realL = L/96;
fprintf('Distance is %d km \n', realL);

%% Exercise 4.4
Le = CalculateLength(sx, sy, 5000);
L100 = CalculateLength(sx, sy, 100);
L200 = CalculateLength(sx, sy, 200);
L400 = CalculateLength(sx, sy, 400);
fprintf('exact distance is: %d \n', Le);
fprintf('delta 100: %d \n', abs(L100-Le));
fprintf('delta 200: %d \n', abs(L200-Le));
fprintf('delta 400: %d \n', abs(L400-Le));

%% Exercise 5.2
DrawFont;
