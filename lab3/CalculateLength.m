function [ L ] = CalculateLength( sx, sy, N )
% a parameter vector
tt = (0:N)/N;
L = norm([ppval(sx,tt(1)) ppval(sy,tt(1))]');
for k=2:N
    temp = [ppval(sx,tt(k))-ppval(sx,tt(k-1)), ppval(sy,tt(k))-ppval(sy,tt(k-1))];
    temp = temp';
    L = L + norm(temp);
end

end

